package fibersfork;

import static fibersfork.Things.lrate;
import java.awt.Color;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class NodeIn extends NodeBox {
  /* ****************************************************** */
  void Assign_Fire(double Value) {
    this.MoteFire = Value;
    if (false) {
      this.PlaneFire = Globals.General_ActFun(Value);
    } else {
      this.PlaneFire = Value;// curve space
    }
  }
  void Assign_MoteNum(int MoteNum) {
    this.MoteNumNow = MoteNum;
  }
  /* ****************************************************** */
  @Override public void Collect_And_Fire() {// Causal
    this.Mote_Prev = this.Mote_Now = null;// force breakage so as not to use
    if (false) {// curve space
      this.MoteFire = Globals.General_ActFun(this.MoteFire);// Do this here?  
    }
  }
  @Override public void Pass_Back_Correctors() {// Causal
  }
  @Override public void Gather_And_Apply_Correctors() {// Causal
  }
  /* ****************************************************** */
  @Override public void Draw_Me(Things.DrawingContext dc) {
    Things.DrawingContext mydc = new Things.DrawingContext();
    mydc.gr = dc.gr;
    mydc.transform.Accumulate(dc.transform, this.xorg, this.yorg, this.xscale, this.yscale);

    PointNd boxmin = new PointNd(2);
    PointNd boxmax = new PointNd(2);
    double xmin = -1.0, ymin = -1.0, xmax = 1.0, ymax = 1.0;
    mydc.transform.To_Screen(xmin, ymin, boxmin);
    mydc.transform.To_Screen(xmax, ymax, boxmax);

    double Red = (this.PlaneFire + 1.0) / 2.0;// now range 0 to 1
    double Blue = 1.0 - Red;

    Color CircleFill = new Color((float) Red, 0.0f, (float) Blue);
    dc.gr.setColor(CircleFill);
    dc.gr.fillOval((int) (boxmin.loc[0]), (int) (boxmin.loc[1]), (int) (boxmax.loc[0] - boxmin.loc[0]), (int) (boxmax.loc[1] - boxmin.loc[1]));
  }
  /* ****************************************************** */
  @Override public NodeIn Clone_Me() {
    NodeIn child = new NodeIn();
    return child;
  }
}
/*
 plan as a dot input
 void Assign_Fire(double Value){
 }
 void Assign_MoteNum(int MoteNum){
 }

 void Collect_And_Fire();
 void Distribute_Outfire();
 void Pass_Back_Correctors();
 void Gather_And_Apply_Correctors();

 */
