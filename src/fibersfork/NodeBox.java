package fibersfork;

import static fibersfork.Things.lrate;
import static fibersfork.Things.ndims_init;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class NodeBox implements Things.Drawable, Things.Causal {
  public Mote_Cloud Motes, Ring_Motes;
  public int Ring_Max = 8, Ring_Last = Ring_Max - 1, RingMoteNumNow = 0;
  public Mote Ring_Mote_Now = null, Ring_Mote_Prev = null;
  public int Num_Us, Num_Ds;// snox do we need this?
  public ArrayList<Axon> US, DS;
  public Roto_Plane planeform;
  public double xorg, yorg, xscale, yscale;
  public double FitError, FitErrorFlywheel = 0.0;
  public Mote Mote_Now = null, Mote_Prev = null;
  public int MoteNumNow;
  double MoteFire, PlaneFire;
  PointNd PlaneHitPoint = null;
  /* ****************************************************** */
  public static class Axon {// link to other Nodes
    public NodeBox US, DS;
    public double Corrector;//, FireVal;
    public double MoteFire, PlaneFire = 0.0;
    public int MoteNum;
    public Axon() {
    }
    public Axon(NodeBox US0, NodeBox DS0) {
      US = US0;
      DS = DS0;
      US0.DS.add(this);
      DS0.US.add(this);
      US0.Num_Ds++;// snox do we need this?
      DS0.Num_Us++;
    }
    public double Get_Outfire() {
      return this.US.Get_Outfire();
    }
  }
  /* ****************************************************** */
  public NodeBox() {
    xscale = yscale = 12.0;
    Num_Us = Num_Ds = 0;
    this.US = new ArrayList<Axon>();
    this.DS = new ArrayList<Axon>();
    if (!Globals.Force_Ring) {
      this.Motes = new Mote_Cloud(this, 0);
    }
    planeform = new Roto_Plane(Globals.DefaultDims);
    this.PlaneHitPoint = new PointNd(2);
    if (Globals.Mote_Ring) {
      this.Init_Mote_Ring(Globals.DefaultDims);
    }
  }
  /* ****************************************************** */
  public void Init_Mote_Ring(int NumDims) {
    this.Ring_Motes = new Mote_Cloud(this, 0);
    Mote mote;
    for (int cnt = 0; cnt < this.Ring_Max; cnt++) {
      mote = new Mote(this, NumDims);
      this.Ring_Motes.add(mote);
      mote.Randomize(-1.0, 1.0);
    }
    this.RingMoteNumNow = 0;
    this.Ring_Mote_Prev = this.Ring_Mote_Now = this.Ring_Motes.get(this.RingMoteNumNow);
  }
  public double Get_Outfire() {
    return -Double.MAX_VALUE;// nonsense placeholder
//    return this.US.Get_Outfire();
  }
  /* ****************************************************** */
  @Override public void Draw_Me(Things.DrawingContext dc) {
    Things.DrawingContext mydc = new Things.DrawingContext();
    mydc.gr = dc.gr;
    mydc.transform.Accumulate(dc.transform, this.xorg, this.yorg, this.xscale, this.yscale);

    PointNd boxmin = new PointNd(2);
    PointNd boxmax = new PointNd(2);
    double xmin = -1.0, ymin = -1.0, xmax = 1.0, ymax = 1.0;
    mydc.transform.To_Screen(xmin, ymin, boxmin);
    mydc.transform.To_Screen(xmax, ymax, boxmax);

    dc.gr.setColor(Color.green);
    dc.gr.drawRect((int) (boxmin.loc[0]), (int) (boxmin.loc[1]), (int) (boxmax.loc[0] - boxmin.loc[0]), (int) (boxmax.loc[1] - boxmin.loc[1]));

    this.planeform.Draw_Me(mydc);

    if (true) {
      PointNd Zero = new PointNd(2);
      mydc.transform.To_Screen(0, 0, Zero);
      PointNd ErrPnt = new PointNd(2);
      mydc.transform.To_Screen(xmin, this.FitError, ErrPnt);
      dc.gr.setColor(Color.black);
      dc.gr.fillRect((int) (boxmax.loc[0]), (int) (Zero.loc[1]), (int) (3), (int) (ErrPnt.loc[1] - Zero.loc[1]));
      dc.gr.setColor(Color.white);
      dc.gr.drawRect((int) (boxmax.loc[0]), (int) (Zero.loc[1]), (int) (3), (int) (ErrPnt.loc[1] - Zero.loc[1]));
    }

    if (!Globals.Force_Ring) {
      for (Mote mote : Motes) {
        mote.Draw_Me(mydc);
      }
//      this.Mote_Now.Draw_Highlight(mydc, Color.green);
    }
    if (true) {
      if (Globals.Mote_Ring) {
        for (Mote mote : this.Ring_Motes) {
          mote.Draw_Me(mydc);
        }
//        this.Ring_Mote_Now.Draw_Highlight(mydc, Color.green);
      }
    }
    if (true) {// plot actual infire as a + 
      int ptsz = 8;
      int halfptsz = ptsz / 2;
      mydc.gr.setColor(Color.green);
      PointNd TicCtr = new PointNd(2);
      mydc.transform.To_Screen(this.PlaneHitPoint.loc[0], this.PlaneHitPoint.loc[1], TicCtr);
      int xmid = (int) (TicCtr.loc[0]);
      int ymid = (int) (TicCtr.loc[1]);

      int x0 = xmid - halfptsz;
      int y0 = ymid - halfptsz;
      int x1 = x0 + ptsz;
      int y1 = y0 + ptsz;
      mydc.gr.drawLine(xmid, y0, xmid, y1);// vertical line
      mydc.gr.drawLine(x0, ymid, x1, ymid);// horizontal line
    }
  }
  public void Init_States(int num_states) {
    if (Globals.Force_Ring) {
      return;
    }
    Mote cpnt;
    double amp = 1.0;
    double arbitrary_output = -0.5;
    for (int pcnt = 0; pcnt < num_states; pcnt++) {
      cpnt = new Mote(this, Globals.DefaultDims);
      cpnt.loc[0] = ((pcnt & 1) - 0.5) * amp;// Arrange xy loc in a Z box shape.
      cpnt.loc[1] = (((pcnt >> 1) & 1) - 0.5) * amp;
      //arbitrary_output = (Logic.wheel.nextDouble() * 2.0) - 1.0;
      cpnt.loc[2] = arbitrary_output;//cpnt.loc[0] + cpnt.loc[1];
      //cpnt.Randomize(-1.0, 1.0);
      this.Motes.add(cpnt);
      arbitrary_output += 0.333;
    }
    this.Mote_Now = this.Motes.get(0);// hacky but must init to something
  }
  public void Init_Random_Unique() {// hacky
    if (Globals.Force_Ring) {
      return;
    }
    double amp = 2.0;
    int Num_Motes = this.Motes.size();
    Mote cpnt;
    double level = 0.0, minjump = 0.1, range = 0.1;
    for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {// random sorted
      cpnt = this.Motes.get(pcnt);
      level += minjump + (Logic.wheel.nextDouble() * range);
      cpnt.loc[cpnt.ninputs] = (((double) level) - 0.5) * amp;
    }
    if (true) {// random shuffled
      double temp;
      int randex;
      Mote cpnt0, cpnt1;
      for (int pcnt = 0; pcnt < Num_Motes; pcnt++) {
        randex = Logic.wheel.nextInt(Num_Motes);
        cpnt0 = this.Motes.get(pcnt);
        cpnt1 = this.Motes.get(randex);
        temp = cpnt0.loc[cpnt0.ninputs];
        cpnt0.loc[cpnt0.ninputs] = cpnt1.loc[cpnt1.ninputs];
        cpnt1.loc[cpnt1.ninputs] = temp;
      }
    }
  }
  public void Init_Unique() {
    if (Globals.Force_Ring) {
      return;
    }
    double amp = 2.0;
    int num_pnts = this.Motes.size();
    int last_pnt = num_pnts - 1;
    Mote cpnt;
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      cpnt = this.Motes.get(pcnt);
      double val = ((double) pcnt) / (double) last_pnt;
      cpnt.Set_Height((((double) val) - 0.5) * amp);
      System.out.println(cpnt.loc[cpnt.ninputs]);
    }
  }
  public void Init_Same(double Value) {// nonsense value, only for testing
    int num_pnts = this.Motes.size();
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      cpnt.loc[cpnt.ninputs] = Value;
    }
  }
  public void Init_Xor() {
    if (Globals.Force_Ring) {
      return;
    }
    double amp = 2.0;
    double value;
    int Num_Motes = this.Motes.size();
    for (int MCnt = 0; MCnt < Num_Motes; MCnt++) {
      Mote mote = this.Motes.get(MCnt);
      int xorval = (MCnt & 1) ^ ((MCnt >> 1) & 1);
      value = (((double) xorval) - 0.5) * amp;
      mote.loc[mote.ninputs] = value;
    }
  }
  public void Init_And() {
    double amp = 2.0;
    int num_pnts = this.Motes.size();
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      int andval = (pcnt & 1) & ((pcnt >> 1) & 1);
      cpnt.loc[cpnt.ninputs] = (((double) andval) - 0.5) * amp;
    }
  }
  public void Init_Or() {
    double amp = 2.0;
    int num_pnts = this.Motes.size();
    for (int pcnt = 0; pcnt < num_pnts; pcnt++) {
      Mote cpnt = this.Motes.get(pcnt);
      int andval = (pcnt & 1) | ((pcnt >> 1) & 1);
      cpnt.loc[cpnt.ninputs] = (((double) andval) - 0.5) * amp;
    }
  }
  public void Line_Up_Motes() {
    if (Globals.Force_Ring) {
      return;
    }
    double amp = 2.0, halfamp = amp / 2.0;
    int Num_Motes = this.Motes.size();
    double value, Num_Spaces = Num_Motes - 1;
    for (int MCnt = 0; MCnt < Num_Motes; MCnt++) {
      Mote mote = this.Motes.get(MCnt);
      value = ((double) MCnt) / Num_Spaces;
      value = (value * amp) - halfamp;
      mote.Fill(value);
    }
  }
  public void Flatten_Motes() {// put all motes flat on curve
    if (Globals.Force_Ring) {
      return;
    }
    double value = 0.0;
    int Num_Motes = this.Motes.size();
    for (int MCnt = 0; MCnt < Num_Motes; MCnt++) {
      Mote mote = this.Motes.get(MCnt);
      value = this.planeform.Get_Plane_Height(mote);
//      value = Globals.General_ActFun(value);
      mote.Set_Height(value);
      mote.Jitter(-0.0001, 0.0001);
    }
  }
  public void ConnectIn(NodeBox upstreamer) {
    this.ConnectInAxon(upstreamer);
    this.Num_Us++;
    upstreamer.Num_Ds++;
  }
  public void ConnectInAxon(NodeBox upstreamer) {
    Axon axon = new Axon(upstreamer, this);
//      upstreamer.DS.add(axon); this.US.add(axon);
  }
  /* ****************************************************** */
  @Override public void Collect_And_Fire() {// Causal
    int InCnt, NumIns = this.US.size();// == this.planeform.ninputs
    Axon InLink;// technically a dendrite
    int MoteNumUS;
    double MoteFireUS, PlaneFireUS;
    PointNd HitPoint = new PointNd(NumIns);
    this.PlaneHitPoint = new PointNd(NumIns);
    this.Mote_Prev = this.Mote_Now;
    for (InCnt = 0; InCnt < NumIns; InCnt++) {
      InLink = this.US.get(InCnt);
      MoteNumUS = InLink.MoteNum;// really freaking wasteful and redundant. value is the same for every iteration.
      MoteFireUS = InLink.MoteFire;
      HitPoint.loc[InCnt] = MoteFireUS;// Just moves my mote to the XY location of my input fires
      PlaneFireUS = InLink.PlaneFire;
      this.PlaneHitPoint.loc[InCnt] = PlaneFireUS;
      this.MoteNumNow = MoteNumUS;// really freaking wasteful and redundant. value is the same for every iteration.
    }

    if (!Globals.Force_Ring) {
      // In the future this.Mote_Now will be chosen by XYZ location, not by index.
      this.Mote_Now = this.Motes.get(this.MoteNumNow);
      this.Mote_Now.Copy_From(HitPoint);// DO NOT copy height, just input dimensions. This relocates the mote.
//    this.Mote_Now.Jitter(-0.001, 0.001);// experiment to avoid getting stuck 

      this.MoteFire = this.Mote_Now.Get_Height();
      if (false) {// curve space (if not in curved space)
        this.MoteFire = Globals.General_ActFun(this.MoteFire);// Do this here?  
      }
    }
    this.PlaneFire = this.planeform.Get_Plane_Height(this.PlaneHitPoint);
    this.PlaneFire = Globals.General_ActFun(this.PlaneFire);// Do this here?

    if (Globals.Mote_Ring) {
      int ClosestNum = -1;
      Mote ClosestMote = null;

      this.Ring_Mote_Prev = this.Ring_Mote_Now;
      ClosestNum = this.Ring_Motes.Find_Closest(HitPoint);
      ClosestMote = this.Ring_Motes.get(ClosestNum);
      this.RingMoteNumNow = (this.RingMoteNumNow < this.Ring_Last) ? this.RingMoteNumNow + 1 : 0;// Get oldest mote.
      this.Ring_Mote_Now = this.Ring_Motes.get(this.RingMoteNumNow);

      if (Math.abs(ClosestMote.Get_Height()) > 0.1) {
        boolean nop = true;// breakpoint
      }

      this.Ring_Mote_Now.Copy_From(HitPoint);// Assign XY loc from axon inputs.
      this.Ring_Mote_Now.Set_Height(ClosestMote.Get_Height());// Assign Z loc from height of closest mote.
//    then we get attraction to plane to pass back?
      /*
       next steps:
       ring motes must receive correctors from downstream and adjust heights
      
       */
      if (Globals.Force_Ring) {
        this.MoteFire = this.Ring_Mote_Now.Get_Height();
      }
    }

//    System.out.println("OutFire:" + this.Mote_Now.Get_Height() + ", Ring:" + this.Ring_Mote_Now.Get_Height());
    if (true) {// disabled for testing
      this.Ping_Plane();
    }
  }
  @Override public void Distribute_Outfire() {// Causal
    int OutCnt, NumOuts = this.DS.size();
    Axon OutLink;
    for (OutCnt = 0; OutCnt < NumOuts; OutCnt++) {
      OutLink = this.DS.get(OutCnt);
      OutLink.MoteNum = this.MoteNumNow;
      OutLink.MoteFire = this.MoteFire;
      OutLink.PlaneFire = this.PlaneFire;
    }
  }
  public void Self_Correct(Mote mote) {
    if (true) {// self-move can be disabled for testing
      mote.Apply_Corrector(mote.Attraction.Get_Height() * lrate);// vfactor
    }
  }
  public void Pass_Back_Correctors(Mote_Cloud Motes, Mote mote) {
    int InCnt, NumIns = this.US.size();// == this.planeform.ninputs
    Roto_Plane plane = this.planeform;
    Axon InLink;// really a dendrite from my point of view
    plane.Find_Closest_On_Curve(mote);// First generate the corrector 
    Motes.Flee(mote);// And generate another corrector to space out the motes
    for (InCnt = 0; InCnt < NumIns; InCnt++) {
      InLink = this.US.get(InCnt);
      InLink.Corrector = mote.Attraction.loc[InCnt];// only apply lrate at the last minute
    }
  }
  @Override public void Pass_Back_Correctors() {// Causal
    Mote Hot_Mote = null;
    Mote_Cloud Cloud = null;
    if (Globals.Force_Ring) {// not compatible with previous locked-together motes approach
      Hot_Mote = this.Ring_Mote_Now;
      Cloud = this.Ring_Motes;
//      if (Globals.Mote_Ring) {// floating motes
//        Pass_Back_Correctors(this.Ring_Motes, this.Ring_Mote_Now);
//        this.Self_Correct(this.Ring_Mote_Now);
//      }
    } else {// locked-together motes
      Hot_Mote = this.Mote_Now;
      Cloud = this.Motes;
    }
    Pass_Back_Correctors(Cloud, Hot_Mote);
    this.Self_Correct(Hot_Mote);
  }
  @Override public void Gather_And_Apply_Correctors() {// Causal
    int DSCnt, NumDS = this.DS.size();
    Axon OutLink;
    double Corrector = 0.0;
    for (DSCnt = 0; DSCnt < NumDS; DSCnt++) {
      OutLink = this.DS.get(DSCnt);
      Corrector += OutLink.Corrector;
    }
    if (false) {
      Corrector *= Globals.sigmoid_deriv(this.Mote_Prev.Get_Height());// ??? do we need this
    }

    if (!Globals.Force_Ring) {
      this.Mote_Prev.Apply_Corrector(Corrector * lrate);// only apply lrate at the last minute
      this.FitError = this.GetFitError(this.Motes);// metrics
    }

    if (Globals.Mote_Ring) {
      this.Ring_Mote_Prev.Apply_Corrector(Corrector * lrate);// only apply lrate at the last minute
//      this.Ring_Mote_Prev.Set_Height(this.Mote_Prev.Get_Height());// junk for confidence test
      this.FitError = this.GetFitError(this.Ring_Motes);// metrics
    }

    this.FitErrorFlywheel = this.FitErrorFlywheel * 0.99 + this.FitError * 0.01;
    if (this.FitErrorFlywheel > 0.1) {
//      this.Explode();
    }
  }
  /* ****************************************************** */
  public void Assign_Ring_Mote_Height(double Height) {
    this.Ring_Mote_Now.Set_Height(Height);
  }
  /* ****************************************************** */
  public double Mote_ActFun(double rawheight) {// virtual
    return rawheight;
  }
  /* ****************************************************** */
  public void Ping_Plane() {
    Mote mote;
    if (Globals.Force_Ring && Globals.Mote_Ring) {// not compatible with locked-together motes
      mote = this.Ring_Mote_Now;
    } else {
      mote = this.Mote_Now;
    }
    this.planeform.Ping(mote);
  }
  public double GetFitError(Mote_Cloud Motes) {
    int NumMotes = Motes.size();
    Mote mote;
    double Height, HgtDiff, Sum = 0.0, Avg, AvgRoot = 0.0;
    for (int pcnt = 0; pcnt < NumMotes; pcnt++) {
      mote = Motes.get(pcnt);
      Height = planeform.Get_Plane_Height(mote);
      if (Double.isNaN(Height)) {
        System.out.println();// breakpoint
      }
      Height = Globals.General_ActFun(Height);
      if (false) {
        HgtDiff = Height - mote.Get_Mapped_Outfire();
      } else {
        HgtDiff = Height - mote.Get_Height();// curve space
      }
      Sum += HgtDiff * HgtDiff;
    }
    Avg = Sum / (double) NumMotes;
    AvgRoot = Math.sqrt(Avg);
    if (Double.isNaN(AvgRoot)) {
      System.out.println();// breakpoint
    }
    return AvgRoot;// Range should be 0.0 to 2.0, (if 2 is node box size).
  }
  public static int ExplodeCnt = 0;
  public String GetDate() {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    return dtf.format(now);
  }
  public void Explode() {// do if node is stubbornly nonlinear
    int NumPnts = Motes.size();
    Mote pnt;
    this.planeform.Randomize(-1.0, +1.0);
    for (int pcnt = 0; pcnt < NumPnts; pcnt++) {
      pnt = Motes.get(pcnt);
      pnt.Randomize(-1.0, +1.0);
    }
    this.FitErrorFlywheel = 0.0;
    System.out.println("Explode! " + GetDate() + ", " + ExplodeCnt);
    ExplodeCnt++;
  }
  public NodeBox Clone_Me() {
    NodeBox child = new NodeBox();
    return child;
  }
}
