package fibersfork;

import static fibersfork.Things.lrate;

/**
 *
 * @author MultiTool
 */
public class NodeOut extends NodeBox {
  /* ****************************************************** */
  @Override public void Distribute_Outfire() {// Causal
    double Score = this.PlaneFire * this.MoteFire;
    System.out.println("Score:" + Score + ", MoteFire:" + this.MoteFire);
//    System.out.println("OutFire:" + this.Mote_Now.Get_Height() + ", Ring:" + this.Ring_Mote_Now.Get_Height());
  }

  @Override public void Pass_Back_Correctors() {// Causal
    if (Globals.Force_Ring) {// not compatible with previous locked-together motes approach
      if (Globals.Mote_Ring) {// floating motes
        Pass_Back_Correctors(this.Ring_Motes, this.Ring_Mote_Now);
      }
    } else {// locked-together motes
      Pass_Back_Correctors(this.Motes, this.Mote_Now);
    }
  }
  /* ****************************************************** */
  @Override public void Gather_And_Apply_Correctors() {// Causal
  }
  /* ****************************************************** */
  @Override public NodeOut Clone_Me() {
    NodeOut child = new NodeOut();
    return child;
  }
}
