package fibersfork;

import java.util.ArrayList;
import java.awt.*;
import java.awt.geom.Point2D;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author MultiTool
 */
public class Things {
  public static int ndims_init = 1;// 3 dimensions.  First dimension is a gimme, becuase it is the output/fire value.
  public static double BoxSpacing = 40.0;
  public static int ColumHeight = 16;
  public static double lrate = 0.1;//0.2;//0.5;//0.9;//0.01;//
  /* ****************************************************** */
  public interface Drawable {
    public void Draw_Me(DrawingContext dc);
  }
  /* ****************************************************** */
  public interface Causal {
    void Collect_And_Fire();
    void Distribute_Outfire();
    void Pass_Back_Correctors();
    void Gather_And_Apply_Correctors();
  }
  /* ****************************************************** */
  public static class TransForm {
    public double xoffs = 0.0, yoffs = 0.0;
    public double xscale = 1.0, yscale = 1.0;
    public void To_Screen(double xloc, double yloc, PointNd answer) {
      answer.loc[0] = xoffs + (xloc * xscale);
      answer.loc[1] = yoffs + (yloc * yscale);
    }
    public void Accumulate(TransForm parent, double xoffsp, double yoffsp, double xscalep, double yscalep) {
      // create my local transform by adding local context to parent context
      // wrong code, just a place holder
      PointNd org = new PointNd(2);
      parent.To_Screen(xoffsp, yoffsp, org);
      xoffs = org.loc[0];
      yoffs = org.loc[1];
      xscale = parent.xscale * xscalep;
      yscale = parent.yscale * yscalep;
    }
  }
  /* ****************************************************** */
  public static class DrawingContext {// Let's be final until we can't anymore
    public Graphics2D gr;
    public TransForm transform;
    public int RecurseDepth;
    /* ********************************************************************************* */
    public DrawingContext() {
      this.RecurseDepth = 0;
      this.transform = new TransForm();
    }
    /* ********************************************************************************* */
    public DrawingContext(DrawingContext Fresh_Parent, TransForm Fresh_Transform) {
      this.gr = Fresh_Parent.gr;
      this.RecurseDepth = Fresh_Parent.RecurseDepth + 1;
    }
    /* ********************************************************************************* */
    public Point2D.Double To_Screen(double XLoc, double YLoc) {
      Point2D.Double pnt = null;
//    pnt = new Point2D.Double(this.GlobalOffset.UnMapTime(XLoc), this.GlobalOffset.UnMapPitch(YLoc));
      return pnt;
    }
    /* ********************************************************************************* */
    public void Compound(Things.TransForm other) {
    }
  }
  /* ****************************************************** */
}
/*
Junkyard

Notes
good results for 128 deep when
. explosions turned off
. vertical attraction turned on
. all motes are init unique non random

 * cycle: find mote vector of attraction to plane tell upstreamer mote
 * heights to go there, they adjust upstreamer mote tells me to go there
 * find vector of attraction to plane again
 *
 *
 * What next? motes ping plane to bend it. motes pass back their desires.
 * when motes receive adjustment now, they must apply it to their heights.
 * no fire forward yet. when motes fire forward, the downstreamer must move
 * its location to match heights of inputs.
 *
 * repaint automatically.
 *
 * need to make special case for input nodes.
 *
 * collect and fire to nowfire (all)
 * for all {
 *   calc desire 
 *   pass back desire (to links)
 * }
 * for all {
 *   collect desire from links
 *   apply desire to my prevfire
 *   push nowfire to links, set prevfire to nowfire.
 * }
 * 
 * each node only has a nowfire.  prev fire is stored in links.
 * 

How to fill an array with exactly N objects randomly distributed, efficiently with no overlaps
int coincnt = 2;
int size = 4;
int numleft = size;
array[size];

for (array){
  double odds = coincnt/numleft;
  if (random(1.0)<odds){
    array[cnt] = true;
    coincnt--;
    if (coincnt<=0){break;}
  }
  numleft--;
}

 */
