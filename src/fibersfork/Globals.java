package fibersfork;

/**
 *
 * @author MultiTool
 */
public class Globals {
  public static int DefaultDims = 3;
  public static boolean Mote_Ring = true, Force_Ring = true;// Force_Ring is for non-backward-compatible testing
  /* ****************************************************** */
  public static double General_ActFun(double rawheight) {
    if (false) {
      return rawheight;
    } else {
      return Globals.ActFun(rawheight);
    }
  }
  /* ****************************************************** */
  public static double ActFun(double xin) {
    double OutVal;// symmetrical sigmoid function in range -1.0 to 1.0. 
    OutVal = xin / Math.sqrt(1.0 + xin * xin);
    return OutVal;
    /*
     * double power = 2.0; OutVal = xin / Math.pow(1 +
     * Math.abs(Math.pow(xin, power)), 1.0 / power);
     */
  }
  /* ****************************************************** */
  public static double Reverse_ActFun(double xin) {
    double OutVal;// from http://www.quickmath.com/webMathematica3/quickmath/page.jsp?s1=equations&s2=solve&s3=basic
    // reverse, inverse of sigmoid is:
    OutVal = xin / (Math.sqrt(Math.abs(xin * xin - 1.0)));
    return OutVal;
  }
  /* ****************************************************** */
  public static double sigmoid_deriv(double Value) {
    /*
     * Given the unit's activation value and sum of weighted inputs, compute
     * the derivative of the activation with respect to the sum. Defined
     * types are SIGMOID (-1 to +1) and ASYMSIGMOID (0 to +1).
     *
     */
    double vsq = Value * Value;// just a half circle
    return Math.sqrt(1.0 - vsq);

//    Value = (Value + 1.0) / 2;
//    double SigmoidPrimeOffset = 0.1;
//    // asymmetrical sigmoid function in range 0.0 to 1.0.
//    double returnval = (SigmoidPrimeOffset + (Value * (1.0 - Value)));
//    returnval *= 2.857142857142857;// modify to fit 1:1 slope of sigmooid
//    return returnval * 1.0;
  }
  /* ****************************************************** */
  public static double sigmoid_deriv_raw(double Value) {
    double vsq = Value * Value;// pre sym sigmoid deriv (from raw sum before actfun) genuine derivative
    double denom1 = Math.pow((vsq + 1.0), (3.0 / 2.0));
    double retval = (1.0 / Math.sqrt(vsq + 1.0)) - (vsq / denom1);
    return retval;
    /*
     http://www.quickmath.com/webMathematica3/quickmath/calculus/differentiate/basic.jsp#c=differentiate_basicdifferentiate&v1=%28x+%2F+sqrt%281.0+%2B+x*x%29%29&v2=x
     pre sym sigmoid deriv (from raw sum before actfun):
     ( 1.0/sqrt(x*x + 1.0) ) - ( x*x / (x*x + 1.0)^(3/2) )
     */
  }
  /* ****************************************************** */
  public static double sigmoid_deriv_postfire(double Value) {
    double MovedValue = (1.0 + Value) / 2.0;// first map range -1 ... +1 to 0 ... +1
    double retval = 2.0 * MovedValue * (1.0 - MovedValue);// APPROXIMATE post sym sigmoid deriv (from fire value after actfun):
    return retval;
    /*
     APPROXIMATE post sym sigmoid deriv (from fire value after actfun):
     4*(x+0.5) * (1.0-(x+0.5))

     output * (1 - output)  derivative of ASYM sigmoid function, after actfun (hillock function)
     */
  }
}
