package fibersfork;

import fibersfork.Things.DrawingContext;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author MultiTool
 */
public class MainGui {
  JFrame frame;
  DrawingPanel drawpanel;
  Layers layers = null;
  /* ********************************************************************************* */
  public MainGui() {
    {
      int Layer_Size = 2;
      Things.ColumHeight = 16;
      Things.ColumHeight = 8;
      layers = new Layers();
//    layers.Make_Layers(2, Layer_Size);
//    layers.Make_Layers(3, Layer_Size);// too tight to work
//    layers.Make_Layers(4, Layer_Size);
//    layers.Make_Layers(6, Layer_Size);
//    layers.Make_Layers(7, Layer_Size);
//    layers.Make_Layers(8, Layer_Size);
//    layers.Make_Layers(12, Layer_Size);
//    layers.Make_Layers(16, Layer_Size);
    layers.Make_Layers(32, Layer_Size);
//    layers.Make_Layers(64, Layer_Size);
//    layers.Make_Layers(128, Layer_Size);
//    layers.Make_Layers(256, Layer_Size);
    }
  }
  /* ********************************************************************************* */
  public void Init() {
    this.frame = new JFrame();
    this.frame.setTitle("Fibers");
//    this.frame.setSize(700, 400);
//    this.frame.setSize(1000, 900);
    this.frame.setSize(1250, 900);
    this.frame.addWindowListener(new WindowAdapter() {
      @Override public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });
    Container contentPane = this.frame.getContentPane();
    this.drawpanel = new DrawingPanel();
    contentPane.add(this.drawpanel);
    this.drawpanel.BigApp = this;
    frame.setVisible(true);
  }
  /* ********************************************************************************* */
  public static class DrawingPanel extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener, ComponentListener, KeyListener {
    MainGui BigApp;
    int ScreenMouseX = 0, ScreenMouseY = 0;
    double MouseOffsetX = 0, MouseOffsetY = 0;

    /* ********************************************************************************* */
    public DrawingPanel() {
      this.Init();
    }
    /* ********************************************************************************* */
    public final void Init() {
      this.addMouseListener(this);
      this.addMouseMotionListener(this);
      this.addMouseWheelListener(this);
      this.addKeyListener(this);
    }
    /* ********************************************************************************* */
    public void Draw_Me(Graphics2D g2d) {
//      System.out.println("MainGui Draw_Me");
      // to do: create paramblob for drawing context, use it in things
      g2d.setBackground(Color.yellow);
      g2d.setBackground(Color.black);
      g2d.clearRect(0, 0, this.getWidth(), this.getHeight());
      Things.TransForm tr = new Things.TransForm();

      int wdt, hgt;

      wdt = this.getWidth();
      hgt = this.getHeight();

      Rectangle2D rect = new Rectangle2D.Float();
      rect.setRect(0, 0, wdt, hgt);

      Stroke oldStroke = g2d.getStroke();
      BasicStroke bs = new BasicStroke(5f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
      g2d.setStroke(bs);
      g2d.setColor(Color.green);
      g2d.draw(rect);// green rectangle confidence check for clipping
      g2d.setStroke(oldStroke);
      DrawingContext dc = new DrawingContext();
      dc.gr = g2d;
      dc.transform = tr;
      {
        Things.DrawingContext mydc = new Things.DrawingContext();
        mydc.gr = dc.gr;
//        mydc.transform.Accumulate(dc.transform, 4, 1, 5.0, 5.0);
//        mydc.transform.Accumulate(dc.transform, 0, 0, 4.0, 4.0);
//        mydc.transform.Accumulate(dc.transform, 0, 0, 3.0, 3.0);
        mydc.transform.Accumulate(dc.transform, 0, 0, 2.0, 2.0);
//        mydc.transform.Accumulate(dc.transform, 0, 0, 1.0, 1.0);
        BigApp.layers.Draw_Me(mydc);
        if (true || BigApp.layers.GenCnt <= 3) {// Freeze action to see initial setup.
          BigApp.layers.RunCycle();
        }
      }
      try {
//        Thread.sleep(2000);
//        Thread.sleep(100);
      } catch (Exception ex) {
      }
//      System.out.println("MainGui Draw_Me EXIT");
    }
    /* ********************************************************************************* */
    @Override public void paintComponent(Graphics g) {
      super.paintComponent(g);
      Graphics2D g2d = (Graphics2D) g;
      Draw_Me(g2d);// redrawing everything is overkill for every little change or move. to do: optimize this
      this.repaint();
//      try {
//        Thread.sleep(100);
//      } catch (Exception ex) {
//      }
    }
    /* ********************************************************************************* */
    @Override public void mouseDragged(MouseEvent me) {
    }
    @Override public void mouseMoved(MouseEvent me) {
      this.ScreenMouseX = me.getX();
      this.ScreenMouseY = me.getY();
    }
    /* ********************************************************************************* */
    @Override public void mouseClicked(MouseEvent me) {
    }
    @Override public void mousePressed(MouseEvent me) {
      this.repaint();
    }
    @Override public void mouseReleased(MouseEvent me) {
      this.repaint();
    }
    @Override public void mouseEntered(MouseEvent me) {
    }
    @Override public void mouseExited(MouseEvent me) {
    }
    /* ********************************************************************************* */
    @Override public void mouseWheelMoved(MouseWheelEvent mwe) {
      double XCtr, YCtr, Rescale;
      XCtr = mwe.getX();
      YCtr = mwe.getY();
      double finerotation = mwe.getPreciseWheelRotation();
      this.repaint();
    }
    /* ********************************************************************************* */
    @Override public void componentResized(ComponentEvent ce) {
    }
    @Override public void componentMoved(ComponentEvent ce) {
    }
    @Override public void componentShown(ComponentEvent ce) {
    }
    @Override public void componentHidden(ComponentEvent ce) {
    }
    /* ********************************************************************************* */
    @Override public void keyTyped(KeyEvent ke) {
    }
    @Override public void keyPressed(KeyEvent ke) {
    }
    @Override public void keyReleased(KeyEvent ke) {
    }
  }

}
