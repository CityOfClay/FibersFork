package fibersfork;

import static fibersfork.Things.BoxSpacing;
import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
/* ****************************************************** */
public class Network implements Things.Drawable, Things.Causal {
  public ArrayList<NodeBox> Node_List;
  public double xorg, yorg;
  /* ****************************************************** */
  public Network() {
    Node_List = new ArrayList<NodeBox>();
  }
  /* ****************************************************** */
  @Override public void Draw_Me(Things.DrawingContext dc) {
//    System.out.println("Network Draw_Me");
    Things.DrawingContext mydc = new Things.DrawingContext();
    mydc.gr = dc.gr;
    mydc.transform.Accumulate(dc.transform, this.xorg, this.yorg, 1.0, 1.0);
    int Num_Nodes = this.Node_List.size();
    NodeBox node;
    for (int NCnt = 0; NCnt < Num_Nodes; NCnt++) {
      node = Node_List.get(NCnt);
      node.Draw_Me(mydc);
    }
//    System.out.println("Network Draw_Me EXIT");
  }
  public void Make_Layer(int num_nodes) {
//    this.Make_Layer(new NodeBox(), num_nodes);
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = new NodeBox();
      nb.Init_States(4);// must rethink this
      nb.xorg = (ncnt + 1.0) * BoxSpacing;
      Node_List.add(nb);
      nb.Line_Up_Motes();
      nb.Flatten_Motes();
    }
  }
  /* ****************************************************** */
  public void Make_Layer(NodeBox... members) {
    int num_nodes = members.length;
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = members[ncnt];
      nb.Init_States(4);// must rethink this
      nb.xorg = (ncnt + 1.0) * BoxSpacing;
      Node_List.add(nb);
    }
  }
  /* ****************************************************** */
  public void Make_Layer(NodeBox factory, int num_nodes) {
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = factory.Clone_Me();
      nb.Init_States(4);// must rethink this
      nb.xorg = (ncnt + 1.0) * BoxSpacing;
      Node_List.add(nb);
    }
  }
  public void Init_Random_Unique() {
    int num_nodes = this.Node_List.size();
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      nb.Init_Random_Unique();
    }
  }
  public void Init_Unique() {
    int num_nodes = this.Node_List.size();
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      nb.Init_Unique();
    }
  }
  public void Init_Xor() {
    int num_nodes = this.Node_List.size();
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      nb.Init_Xor();
    }
  }
  public void Init_And() {
    int num_nodes = this.Node_List.size();
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      nb.Init_And();
    }
  }
  public void Init_Or() {
    int num_nodes = this.Node_List.size();
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      nb.Init_Or();
    }
  }
  public void Connect_Weave(Network other) {
    // Connect two meshes in a weave
    int num_my_nodes = this.Node_List.size();
    int num_other_nodes = other.Node_List.size();
    if (num_my_nodes != num_other_nodes) {
      // boom!  networks have to be the same size
      System.exit(1);
    }

    NodeBox us, ds;
    int prevcnt = num_my_nodes - 1;// loop 

    for (int cnt = 0; cnt < num_my_nodes; cnt++) {
      ds = this.Node_List.get(cnt);

      us = other.Node_List.get(prevcnt);
      ds.ConnectIn(us);

      us = other.Node_List.get(cnt);
      ds.ConnectIn(us);

      prevcnt = cnt;
    }
  }
  public void Connect_From_Other(Network other) {
    // Connect all-to-all between two meshes
    int num_my_nodes = this.Node_List.size();
    int num_other_nodes = other.Node_List.size();

    for (int ncnt0 = 0; ncnt0 < num_other_nodes; ncnt0++) {
      NodeBox us = other.Node_List.get(ncnt0);
      for (int ncnt1 = 0; ncnt1 < num_my_nodes; ncnt1++) {
        NodeBox ds = this.Node_List.get(ncnt1);
        ds.ConnectIn(us);
      }
    }
    for (int ncnt1 = 0; ncnt1 < num_my_nodes; ncnt1++) {
      NodeBox ds = this.Node_List.get(ncnt1);
      //ds.Init_States(4);// must rethink this
      //ds.Init_States(1 << ds.Num_Us);// wrong wrong wrong
    }
  }
  @Override public void Collect_And_Fire() {// Causal
    int num_my_nodes = this.Node_List.size();
    for (int NodeCnt = 0; NodeCnt < num_my_nodes; NodeCnt++) {
      NodeBox nb = this.Node_List.get(NodeCnt);
      nb.Collect_And_Fire();
    }
  }
  @Override public void Distribute_Outfire() {// Causal
    int num_my_nodes = this.Node_List.size();
    for (int NodeCnt = 0; NodeCnt < num_my_nodes; NodeCnt++) {
      NodeBox nb = this.Node_List.get(NodeCnt);
      nb.Distribute_Outfire();
    }
  }
  @Override public void Pass_Back_Correctors() {// Causal
    int num_my_nodes = this.Node_List.size();
    for (int ncnt1 = 0; ncnt1 < num_my_nodes; ncnt1++) {
      NodeBox nb = this.Node_List.get(ncnt1);
      nb.Pass_Back_Correctors();
    }
  }
  @Override public void Gather_And_Apply_Correctors() {// Causal
    int num_my_nodes = this.Node_List.size();
    for (int ncnt1 = 0; ncnt1 < num_my_nodes; ncnt1++) {
      NodeBox nb = this.Node_List.get(ncnt1);
      nb.Gather_And_Apply_Correctors();
    }
  }
  public double Get_Avg_FitError() {
    int num_nodes = this.Node_List.size();
    double Sum = 0.0;
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      Sum += nb.FitError;
    }
    return Sum / (double) num_nodes;
  }
  /* ****************************************************** */
  public void Assign_Ring_Mote_Height(double Height) {
    int num_nodes = this.Node_List.size();
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      NodeBox nb = this.Node_List.get(ncnt);
      nb.Assign_Ring_Mote_Height(Height);
    }
  }
}
